import List from './List';
import React from 'react';

class Locations extends List {
  componentDidMount() {
    const listName = 'locations';
    var locations = localStorage.getItem(listName) ? JSON.parse(localStorage.getItem(listName)) : [];
    var i;
    var items = [];
    for (i=0; i<locations.length; i++) {
      var location = locations[i];
      items.push(
        <div id={i} className='row-div' onClick={this.handleSelectRow.bind(this)}>
          {location.name}
          <br />
          {location.address}
          <br />
          {location.selectedCategory}
          <br />
          <br />
        </div>
      );
    }
    this.setState({items: items, currentListName: listName});
  }
}

export default Locations;
