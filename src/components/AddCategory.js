import React from 'react';
import AddForm from './AddForm';

class AddCategory extends AddForm {
  componentDidMount() {
    this.setCachedValues('categories');
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit} list='categories'>
        <table>
          <tbody>
              <tr key={0}>
                <td>
                  <div className='row-div'>
                    <input name='categoryName' type='text' id='categoryName' placeholder='Name' value={this.state.values['categoryName']} onChange={this.handleChange} />
                  </div>
                </td>
              </tr>
              <div className='form-group'>
                <input type='submit' value={this.state.inEditMode ? 'Save' : 'Submit'} />
              </div>
          </tbody>
        </table>
      </form>
    );
  }
}

export default AddCategory;
