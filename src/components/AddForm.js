import { Component } from 'react';

class AddForm extends Component {
  constructor() {
    super();
    this.state = {
      fields: [],
      values: {},
      currentListName: '',
      inEditMode: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setCachedValues(currentListName) {
    if (this.props.isEditing) {
      var listArray = JSON.parse(localStorage.getItem(currentListName));
      var index = parseInt(this.props.selectedRowIndex);
      var cachedItem = listArray[index];
      this.props.onEdit();
      this.setState({values: cachedItem, inEditMode: true});
    }
  }

  handleChange(event) {
    var values = this.state.values;
    values[[event.target.name]] = event.target.value;
    this.setState({values: values});
  }

  handleSubmit(event) {
    event.preventDefault();
    var listName = event.currentTarget.attributes.getNamedItem('list').value;
    var values = this.state.values;

    // validate there're no empty fields
    var valuesCount = Object.keys(values).length;
    var invalidFields = valuesCount === 0 || (listName === 'locations' && valuesCount < 3);
    for (let [key] of Object.entries(values)) {
      if (values[[key]].length === 0) {
        invalidFields = true;
        break;
      }
    }
    if (invalidFields) {
      alert('Please fill all the fields');
      return;
    }

    if (listName === 'locations') {
      this.getAddressCoordinates(listName);
    } else {
      this.saveItemToLocalStorage(listName);
    }
  }

  getAddressCoordinates(listName) {
    var fullAddress = this.state.values.address;
    if (fullAddress && fullAddress.length > 0) {
      fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+fullAddress)
      .then((response) => response.json())
      .then((responseJson) => {
        var results = responseJson.results;
        if (results['0'] && results['0']['geometry']) {
          this.state.values.location = results['0']['geometry']['location'];
          this.saveItemToLocalStorage(listName);
        } else {
          alert('Please enter a valid address');
        }
      })
      .catch((error) => {
        alert(error);
      })
    }
  }

  saveItemToLocalStorage(listName) {
    // create an array if it doesn't exist yet in the localStorage
    var listArray = [];
    if (!localStorage.getItem([listName])) {
     localStorage.setItem([listName], JSON.stringify(listArray));
    }
    listArray = JSON.parse(localStorage.getItem([listName]));
    var newData = {};
    for (let [key] of Object.entries(this.state.values)) {
      newData[[key]] = this.state.values[[key]];
    }

    // save editings in existing item or push a new one
    if (this.state.inEditMode) {
      var index = parseInt(this.props.selectedRowIndex);
      listArray[index] = newData;
    } else {
      listArray.push(newData);
    }
    localStorage.setItem([listName], JSON.stringify(listArray));
    // event.preventDefault();
  }
}

export default AddForm;
