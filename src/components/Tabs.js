import React, { Component } from 'react';
import Beforeunload from 'react-beforeunload';
import {saveState, loadState} from '../Helper';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import '../css/Tabs.css';

const titles = ['Categories', 'Locations'];

class Tabs extends Component {
  constructor() {
    super();
    const savedState = loadState('Tabs');
    if (savedState) {
      this.state = savedState;
    } else {
      this.state = {
        selectedTabIndex: 0,
        title: titles[0],
        selectedRowIndex: -1
      };
    }
  }

  handleSelectTab(tabIndex) {
    var rowIndex;
    if (this.state.selectedTabIndex === tabIndex) {
      rowIndex = this.state.selectedRowIndex;
    } else {
      rowIndex = -1;
    }
    this.setState({
       selectedTabIndex: tabIndex,
       title: titles[tabIndex],
       selectedRowIndex: rowIndex
     });
  }

  handleSelectedRow(index) {
    this.setState({selectedRowIndex: index});
  }

  handleBack(title) {
    this.setState({ title: title });
  }

  handleAdd() {
    const title = this.state.selectedTabIndex === 0 ? 'Add Category' : 'Add Location';
    this.setState({ title: title});
  }

  handleEdit() {
    this.child.editItem();
    const title = this.state.selectedTabIndex === 0 ? 'Edit Category' : 'Edit Location';
    this.setState({ title: title});
  }

  handleWatchMap() {
    this.child.watchMap(this.state.selectedRowIndex);
    this.setState({ title: 'Map'});
  }

  handleDelete() {
    this.child.deleteItem();
  }

  render() {
    return (
      <div>
        <Beforeunload onBeforeunload={saveState(this.state, 'Tabs')} />

        <Header ref={instance => {this.child = instance; }}
         tabIndex={this.state.selectedTabIndex}
         title={this.state.title}
         selectedRowIndex={this.state.selectedRowIndex}
         onAdd={this.handleAdd.bind(this)}
         onEdit={this.handleEdit.bind(this)}
         onDelete={this.handleDelete.bind(this)}
         onBack={this.handleBack.bind(this)}
         onWatchMap={this.handleWatchMap.bind(this)}
        />

        <Main ref={instance => {this.child = instance; }}
         onSelectedRow={this.handleSelectedRow.bind(this)}
        />

        <Footer titles={titles} onSelectTab={this.handleSelectTab.bind(this)} />
      </div>
    );
  }
}

export default Tabs;
