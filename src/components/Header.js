import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Beforeunload from 'react-beforeunload';
import {saveState, loadState} from '../Helper';
import addIcon from '../images/ic_add.png';
import deleteIcon from '../images/ic_delete.png';
import editIcon from '../images/ic_edit.png';
import backIcon from '../images/ic_arrow_back.png';
import placeIcon from '../images/ic_place.png';
import '../css/Tabs.css';

class Header extends Component {
  constructor() {
    super();
    const savedState = loadState('Header');
    if (savedState) {
      this.state = savedState;
    } else {
      this.state = {
        showBackButton: false,
        lastPageTitle: ''
      };
    }
  }

  add() {
    this.setState({
      showBackButton: true,
      lastPageTitle: this.props.title
    });
    this.props.onAdd();
  }

  delete() {
    this.props.onDelete();
  }

  edit() {
    this.setState({
      showBackButton: true,
      lastPageTitle: this.props.title
    });
    this.props.onEdit();
  }

  viewMap() {
    this.setState({
      showBackButton: true,
      lastPageTitle: this.props.title
    });
    this.props.onWatchMap();
  }

  back() {
    this.setState({showBackButton: false});
    this.props.onBack(this.state.lastPageTitle);
  }

  componentWillReceiveProps() {
    if (this.state.showBackButton) {
      this.back();
    }
  }

  render() {
    const disabled = this.props.selectedRowIndex === -1;
    const deleteDisabled = disabled ? null : this.delete.bind(this);
    const editDisabled = disabled ? null : this.edit.bind(this);
    const viewMapDisabled = disabled ? null : this.viewMap.bind(this);
    const buttonStyle = disabled ? {'opacity': '0.5'} : {'opacity': '1.0'}
    const linkClassName = disabled ? 'disabled-link' : '';
    const linkToList = this.props.tabIndex === 0 ? '/Categories': '/Locations'
    const linkToForm = this.props.tabIndex === 0 ? '/AddCategory': '/AddLocation';
    return (
      <nav>
      <Beforeunload onBeforeunload={saveState(this.state, 'Header')} />
        <div id='left-nav-div'>
          { this.state.showBackButton ?
          <div>
            <Link to={linkToList}
               onClick={this.back.bind(this)}>
                <img src={backIcon} alt='' />
            </Link>
          </div>
          : null }
        </div>
        <div id='nav-title'><h3>{this.props.title}</h3></div>
        <div id='right-nav-div'>

        { (!this.state.showBackButton && this.props.tabIndex !== 0) ?
          <div>
          <Link to={'/Map'}
            style={buttonStyle} className={linkClassName} onClick={viewMapDisabled}>
              <img src={placeIcon} alt='' />
            </Link>
          </div>
          : null }

        { !this.state.showBackButton ?
          <div>
          <Link to={linkToList}
            style={buttonStyle} className={linkClassName} onClick={deleteDisabled}>
              <img src={deleteIcon} alt='' />
            </Link>
          </div>
          : null }

          { !this.state.showBackButton ?
            <div>
              <Link to={linkToForm} style={buttonStyle} onClick={editDisabled}>
                <img src={editIcon} alt='' />
              </Link>
            </div>
            : null }

          { !this.state.showBackButton ?
            <div>
              <Link to={linkToForm}
                 onClick={this.add.bind(this)}>
                  <img src={addIcon} alt='' />
              </Link>
            </div>
            : null }
        </div>
      </nav>
    );
  }
}

export default Header;
