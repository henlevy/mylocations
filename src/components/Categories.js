import List from './List';
import React from 'react';

class Categories extends List {
  componentDidMount() {
    const listName = 'categories';
    var categories = localStorage.getItem(listName) ? JSON.parse(localStorage.getItem(listName)) : [];
    var i;
    var items = [];
    for (i=0; i<categories.length; i++) {
      items.push(
        <div id={i} className='row-div' onClick={this.handleSelectRow.bind(this)}>
          {categories[i].categoryName}
          <br />
          <br />
        </div>
      );
    }
    this.setState({items: items, currentListName: listName});
  }
}

export default Categories;
