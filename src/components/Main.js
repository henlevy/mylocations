import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Beforeunload from 'react-beforeunload';
import {saveState, loadState} from '../Helper';
import Categories from './Categories';
import Locations from './Locations';
import AddCategory from './AddCategory';
import AddLocation from './AddLocation';
import Map from './Map';
import '../css/Tabs.css';

class Main extends Component {
  constructor() {
    super();
    const savedState = loadState('Main');
    if (savedState) {
      this.state = savedState;
    } else {
      this.state = {
        isDeleting: false,
        isEditing: false,
        selectedRowIndex: -1
      };
    }
  }

  handleSelectRow(index) {
    this.props.onSelectedRow(index);
    this.setState({selectedRowIndex: index});
  }

  deleteItem() {
    this.setState({isDeleting: true});
  }

  itemDeleted() {
    this.setState({isDeleting: false});
  }

  editItem() {
    this.setState({isEditing: true});
  }

  itemEdited() {
    this.setState({isEditing: false});
  }

  watchMap(index) {
    this.setState({selectedRowIndex: index});
  }

  render() {
    return (
      <div className='Main'>
      <Beforeunload onBeforeunload={saveState(this.state, 'Main')} />
        <Switch>
          <Route
          exact path="/Categories"
          render={
            props =>
            <Categories
             onSelectedRow={this.handleSelectRow.bind(this)}
             isDeleting={this.state.isDeleting}
             onDeleted={this.itemDeleted.bind(this)}
             />}
          />
          <Route
          exact path="/Locations"
          render={props =>
            <Locations
             onSelectedRow={this.handleSelectRow.bind(this)}
             isDeleting={this.state.isDeleting}
             onDeleted={this.itemDeleted.bind(this)}
             />}
          />
          <Route exact path='/AddCategory' render={props =>
            <AddCategory
             isEditing={this.state.isEditing}
             onEdit={this.itemEdited.bind(this)}
             selectedRowIndex={this.state.selectedRowIndex} />}
           />
           <Route exact path='/AddLocation' render={props =>
             <AddLocation
              isEditing={this.state.isEditing}
              onEdit={this.itemEdited.bind(this)}
              selectedRowIndex={this.state.selectedRowIndex} />}
            />
            <Route exact path='/Map' render={props =>
              <Map selectedRowIndex={this.state.selectedRowIndex} />}
             />
          <Redirect from="/" to="/Categories" />
        </Switch>
      </div>
    );
  }
}

export default Main;
