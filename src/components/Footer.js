import React, { Component } from 'react';
import Beforeunload from 'react-beforeunload';
import {saveState, loadState} from '../Helper';
import TabItem from './TabItem';
import '../css/Tabs.css';

class Footer extends Component {
  constructor() {
    super();
    const savedState = loadState('Footer');
    if (savedState) {
      this.state = savedState;
    } else {
      this.state = {
        selectedTabIndex: 0
      };
    }
  }

  handleSelectTab(tabIndex) {
    this.setState({selectedTabIndex: tabIndex});
    this.props.onSelectTab(tabIndex);
  }

  render() {
    const { slideStyle, lineStyle } = styles;
    const isLeft = this.state.selectedTabIndex === 0;
    const left = isLeft ? '0' : '50%';
    const right = isLeft ? '50%' : '0';
    const newSlideStyle = {...slideStyle, left: left, right: right};
    return (
      <footer>
      <Beforeunload onBeforeunload={saveState(this.state, 'Footer')} />
        {this.props.titles.map((title, index) =>
          <TabItem key={index} index={index} title={title} onSelect={this.handleSelectTab.bind(this)} />
        )}
        <div style={newSlideStyle}>
          <div style={lineStyle}></div>
        </div>
      </footer>
    );
  }
}

const styles = {
  slideStyle: {
    background: '#ABEBC6',
    width: '50%',
    height: '4px',
    position: 'absolute',
    top: 'calc(100% - 16px)',
    left: '0',
    right: '50%',
    transition: 'left 0.2s linear',
    MozTransition: 'left 0.2s linear', /* Firefox */
    WebkitTransition: 'left 0.2s linear' /* Safari and Chrome */,
    display: '-webkit-flex', /* Safari */
    display: 'flex',
    WebkitJustifyContent: 'center', /* Safari */
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  lineStyle: {
    background: '#FF5733',
    top: '0',
    width: '80px',
    bottom: '0',
    position: 'absolute',
    height: '4px'
  }
};

export default Footer;
