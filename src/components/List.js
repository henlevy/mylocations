import React, { Component } from 'react';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      selectedRowIndex: -1,
      currentListName: ''
    };
    this.deleteItem = this.deleteItem.bind(this);
  }

  handleSelectRow(event) {
    if (event.target.id) {
      const index = event.target.id;
      this.props.onSelectedRow(index);
      this.setState({selectedRowIndex: index});
    }
  }

  componentDidUpdate() {
    if (this.props.isDeleting) {
      this.deleteItem();
    }
  }

  deleteItem() {
    if (this.state.selectedRowIndex !== -1) {
      const currentListName = this.state.currentListName;
      var listArray = JSON.parse(localStorage.getItem(currentListName));
      listArray.splice(this.state.selectedRowIndex, 1);
      localStorage.setItem(currentListName, JSON.stringify(listArray));
      this.props.onDeleted();
      this.componentDidMount();
      this.setState({selectedRowIndex: -1});
    }
  }

  render() {
    return (
      <section>
        <table>
          <tbody>
            {
              this.state.items.map((item, index) =>
              <tr key={index} style={index == this.state.selectedRowIndex ? styles.trSelectedStyle : styles.trUnselectedStyle} onClick={this.handleSelectRow.bind(this)}><td>{item}</td></tr>)
            }
          </tbody>
        </table>
      </section>
    );
  }
}

const styles = {
  trSelectedStyle: {
    backgroundColor: '#ff8970'
  },
  trUnselectedStyle: {
    backgroundColor: 'white'
  }
}

export default List;
