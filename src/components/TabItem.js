import React, { Component } from 'react';
import { Link } from 'react-router-dom'
class TabItem extends Component {

  handleClick() {
    this.props.onSelect(this.props.index);
  }

  render() {
    const link = this.props.index === 0 ? '/Categories' : '/Locations';
    return (
      <Link to={link}
         className='tab-item'
         onClick={this.handleClick.bind(this)}>
          {this.props.title}
       </Link>
    );
  }
}

export default TabItem;
