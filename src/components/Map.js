import React, { Component } from 'react';
import _ from 'lodash';
import {withGoogleMap, GoogleMap, Marker, InfoWindow} from 'react-google-maps/lib';
import withScriptjs from 'react-google-maps/lib/async/withScriptjs';

var GMS_API_KEY = 'AIzaSyCAxkx5kYPAjiEneZEo1Em_Sn0WJsE1QLs';

const AsyncGettingStartedExampleGoogleMap = _.flowRight(
  withScriptjs,
  withGoogleMap,
)(props => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={14}
    defaultCenter={props.markers[0].position}
    onClick={props.onMapClick}
  >
    {props.markers.map(marker => (
      <Marker
        {...marker}
        onClick={() => props.handleMarkerClick(marker)}
      >
      { marker.showInfoWindow ?
        <InfoWindow onCloseClick={this.handleMarkerClose}>
          <div className='infoWindowDiv'>
            <h5><b>{marker.details.name}</b></h5>
            Address: {marker.details.address}
            <br />
          </div>
        </InfoWindow> : null
      }
      </Marker>
    ))}
  </GoogleMap>
));

class Map extends Component {

  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      width: '0',
      height: '0',
      initialLoadingDone: false
    }
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);

    var listArray = JSON.parse(localStorage.getItem('locations'));
    var index = parseInt(this.props.selectedRowIndex);
    var place = listArray[index];
    const details = {
     name: place.name,
     address: place.address
    };
    var newMarker = {
      position: place.location,
      key: 0,
      details: details,
      showInfoWindow: true
    }
    var markers = this.state.markers;
    markers.push(newMarker);
    this.setState({
      markers: markers
    });
  }

  handleMapLoad = this.handleMapLoad.bind(this);
  handleMarkerClick = this.handleMarkerClick.bind(this);

  handleMapLoad(map) {
    this._mapComponent = map;
    if (map) {
      // console.log(map.getZoom());
    }
  }

  handleMarkerClick(targetMarker) {
    targetMarker.showInfoWindow = !targetMarker.showInfoWindow;
    this.setState({
      markers: this.state.markers
    });
  }

  render() {
    var height = this.state.height - 50;
    return (
      <div>
        <AsyncGettingStartedExampleGoogleMap
          googleMapURL={'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key='+GMS_API_KEY}
          loadingElement={
            <div style={{ height: height+'px' }}></div>
          }
          containerElement={
            <div style={{ height: height+'px' }} />
          }
          mapElement={
            <div style={{ height: height+'px' }} />
          }
          onMapLoad={this.handleMapLoad}
          markers={this.state.markers}
          handleMarkerClick={this.handleMarkerClick}
        />
      </div>
    );
  }
}

export default Map;
