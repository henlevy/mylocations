import React from 'react';
import AddForm from './AddForm';

class AddLocation extends AddForm {
  constructor() {
    super();
    var cachedCategories = [];
    var selectedCategory = '';
    if (localStorage.getItem('categories')) {
      var categories = JSON.parse(localStorage.getItem('categories'));
      var i;
      var items = [];
      for (i=0; i<categories.length; i++) {
        items.push(categories[i].categoryName);
      }
      cachedCategories = items;
      selectedCategory = items[0];
    }
    this.state = {
      categories: cachedCategories,
      values: {name: '', address: '', selectedCategory: selectedCategory}
    }
  }

  componentDidMount() {
    this.setCachedValues('locations');
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} list='locations'>
        <table>
          <tbody>
              <tr key={0}>
                <td className='td-form'>
                  <div className='row-div'>
                    <input name='name' type='text' id='name' placeholder='Name' value={this.state.values['name']} onChange={this.handleChange} />
                  </div>
                </td>
              </tr>

              <tr key={1}>
                <td className='td-form'>
                  <div className='row-div'>
                    <input name='address' type='text' id='address' placeholder='Address' value={this.state.values['address']} onChange={this.handleChange} />
                  </div>
                </td>
              </tr>

        <tr key={2}>
          <td className='td-form'>
            <div className='row-div'>
              <span>Select Category:  </span>
              <select
              name='selectedCategory'
              value={this.state.values['selectedCategory']} onChange={this.handleChange} >
                {
                  this.state.categories.map((categoryName, index) =>
                  <option key={index} value={categoryName}>{categoryName}</option>
                )}
              </select>
            </div>
          </td>
        </tr>

        <div className='form-group'>
          <input type='submit' value={this.state.inEditMode ? 'Save' : 'Submit'} />
        </div>
        </tbody>
      </table>
    </form>
    );
  }
}

export default AddLocation;
