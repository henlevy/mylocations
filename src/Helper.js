
export const loadState=(key)=>{
  try {
    const serializedState = localStorage.getItem(key);
     if (serializedState === null) {
       return null; // reducer will return Redux state, as localstorage is null.
     }
    return JSON.parse(serializedState);
  } catch (err) {
    return null;
  }
}

export const saveState=(state, key)=>{
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(key, serializedState);
  } catch (err) {
    // ignore error
  }
}
